
//BOTONES
var consola = document.getElementById('console').getElementsByClassName('body')[0];

var addV3Btn = document.getElementById("addV3Btn");
addV3Btn.addEventListener('click', addVectors);

var moduloV3Btn = document.getElementById("moduloV3Btn");
moduloV3Btn.addEventListener('click', moduloVectors);

var anguloV3Btn = document.getElementById("anguloV3Btn");
anguloV3Btn.addEventListener('click', anguloVectors);

var substractV3Btn = document.getElementById("substractV3Btn");
substractV3Btn.addEventListener('click', substractVectors);

var multiplicacionV3Btn = document.getElementById("multiplicacionV3Btn");
multiplicacionV3Btn.addEventListener('click', multiConst);

var productoPuntoV3Btn = document.getElementById("productoPuntoV3Btn");
productoPuntoV3Btn.addEventListener('click', productoPuntoMain);

var productoCruzV3Btn = document.getElementById("productoCruzV3Btn");
productoCruzV3Btn.addEventListener('click', productoCruzMain);



//FUNCIONES VECTOR 3D


function anguloVectors(){
	var u = crearVector();
	anguloUxV = u.angulos();
	consola.innerHTML += "<p> >> Angulo de los vectores  </p>";
	consola.innerHTML += "<p> >> Vector3 u: "+JSON.stringify ( u ) + " </p>";
	consola.innerHTML += "<p> >> Vector3 angulo (x): "+JSON.stringify ( anguloUxV.x ) + " </p>";
	consola.innerHTML += "<p> >> Vector3 angulo (y): "+JSON.stringify ( anguloUxV.y ) + " </p>";
	consola.innerHTML += "<p> >> Vector3 angulo (z): "+JSON.stringify ( anguloUxV.z ) + " </p>";

}
function multiConst (){
	var u = crearVector();
	var c = crearConstante();
	multiConstante = u.multiplicacionPorConstante(c);
	consola.innerHTML += "<p> >> Multiplicacion por Constante de los vectores  </p>";
	consola.innerHTML += "<p> >> Vector3 u: "+JSON.stringify ( u ) + " </p>";
	consola.innerHTML += "<p> >> Constante: "+JSON.stringify ( c ) + " </p>";
	consola.innerHTML += "<p> >> Vector3 Multiplicacion: "+JSON.stringify ( multiConstante ) + " </p>";
}

function productoPuntoMain (){
	var u = crearVector();
	var v = crearVector();
	var punto = u.productoPunto(v);
	consola.innerHTML += "<p> >> Producto Punto de vectores </p>";
	consola.innerHTML += "<p> >> Vector3 u : "+JSON.stringify ( u ) + " </p>";
	consola.innerHTML += "<p> >> Vector3 v : "+JSON.stringify ( v ) + " </p>";
	consola.innerHTML += "<p> >> Vector3 resultante : "+JSON.stringify ( punto ) + " </p>";
}
function productoCruzMain (){
	var u = crearVector();
	var v = crearVector();
	var cruz = u.productoCruz(v);
	consola.innerHTML += "<p> >> Producto Cruz de vectores </p>";
	consola.innerHTML += "<p> >> Vector3 u: "+JSON.stringify ( u ) + " </p>";
	consola.innerHTML += "<p> >> Vector3 v: "+JSON.stringify ( v ) + " </p>";
	consola.innerHTML += "<p> >> Vector3 resultante : "+JSON.stringify ( cruz ) + " </p>";

}

function moduloVectors (){
	var u = crearVector();
	moduloUxV = u.moduloF();
	consola.innerHTML += "<p> >> Modulo de los vectores  </p>";
	consola.innerHTML += "<p> >> Vector3 u: "+JSON.stringify ( u ) + " </p>";
	consola.innerHTML += "<p> >> Vector3 modulo: "+JSON.stringify ( moduloUxV ) + " </p>";
}
function substractVectors(){

	var u = crearVector();
	var v = crearVector();
	console.log(u);
	console.log(v);
	var u_subs_v = u.substract(v);
	
	consola.innerHTML += "<p> >> Resta de vectores </p>";
	consola.innerHTML += "<p> >> Vector3 u: "+JSON.stringify ( u ) + " </p>";
	consola.innerHTML += "<p> >> Vector3 v: "+JSON.stringify ( v ) + " </p>";
	consola.innerHTML += "<p> >> Vector3 u-v: "+JSON.stringify ( u_subs_v ) + " </p>";
	
}
function addVectors(){

	var u = crearVector();
	var v = crearVector();
	console.log(u);
	console.log(v);
	var u_add_v = u.add(v);
	
	consola.innerHTML += "<p> >> Suma de vectores </p>";
	consola.innerHTML += "<p> >> Vector3 u: "+JSON.stringify ( u ) + " </p>";
	consola.innerHTML += "<p> >> Vector3 v: "+JSON.stringify ( v ) + " </p>";
	consola.innerHTML += "<p> >> Vector3 u+v: "+JSON.stringify ( u_add_v ) + " </p>";
	
}
function crearConstante(){
	var message = 'Digite la constate'
	var value = prompt(message,"");
	var  constante=parseFloat(value);
	return constante;;

}


function crearVector(){

	var message = 'Digite las componentes del vector 3D separadas\
	por comas \",\" o un escalar para asignar a las\
	3 componentes';

	var value = prompt(message,"0,0,0");

	if( value != null){
		value = value.split(",");
		for (var i = 0; i < value.length; i++) {
			value[i] = parseFloat(value[i]);
		};

		if(value.length > 1){
			return new Vector3(value[0],value[1],value[2]);
		}else{
			return new Vector3(value[0]);
		}
	}
}


//MATRICES 2x2
var masumo2x2 = new Matrix2(null,null,null,null);
//Botones
var btnSumaMatris2x2= document.getElementById("btnSumaMatris2x2");
btnSumaMatris2x2.addEventListener('click', sumaDeMatris2x2);

var btnRestaMatris2x2= document.getElementById("btnRestaMatris2x2");
btnRestaMatris2x2.addEventListener('click', restaDeMatris2x2);

var btnMultiConstanMatris2x2= document.getElementById("btnMultiConstanMatris2x2");
btnMultiConstanMatris2x2.addEventListener('click', multipliXConstante2x2);

var btnMultiplicacionMatris2x2= document.getElementById("btnMultiplicacionMatris2x2");
btnMultiplicacionMatris2x2.addEventListener('click', multiplicacionMatrices2x2);

//Funciones

function sumaDeMatris2x2(){
	
	var u = crearMatris2x2();
	var v = crearMatris2x2();
	masumo2x2.sumaMatrices2x2(u,v);
	consola.innerHTML += "<p> >> Matris 1 v:"+JSON.stringify (u) + " </p>";
	consola.innerHTML += "<p> >> Matris 2 v:"+JSON.stringify (v) + " </p>";
	
	consola.innerHTML += "<p> >> Resultado:"+JSON.stringify (masumo2x2.sumaMatrices2x2(u,v)) + " </p>";
}
function restaDeMatris2x2(){
	
	var u = crearMatris2x2();
	var v = crearMatris2x2();
	masumo2x2.restaMatrices2x2(u,v);
	consola.innerHTML += "<p> >> Matris 1 v:"+JSON.stringify (u) + " </p>";
	consola.innerHTML += "<p> >> Matris 2 v:"+JSON.stringify (v) + " </p>";
	
	consola.innerHTML += "<p> >> Resultado:"+JSON.stringify (masumo2x2.restaMatrices2x2(u,v))+ " </p>";
}

function multipliXConstante2x2(){
	var u = crearMatris2x2();
	var v = crearConstante();
	masumo2x2.multipli_constante2x2(u,v);
	consola.innerHTML += "<p> >> Matris 1 v:"+JSON.stringify (u) + " </p>";
	consola.innerHTML += "<p> >> Constante:"+JSON.stringify (v) + " </p>";
	
	consola.innerHTML += "<p> >> Resultado:"+JSON.stringify (masumo2x2.multipli_constante2x2(u,v)) + " </p>";
}

function multiplicacionMatrices2x2(){
	var u = crearMatris2x2();
	var v = crearMatris2x2();
	masumo2x2.multipli2x2(u,v);
	consola.innerHTML += "<p> >> Matris 1 v:"+JSON.stringify (u) + " </p>";
	consola.innerHTML += "<p> >> Constante:"+JSON.stringify (v) + " </p>";
	
	consola.innerHTML += "<p> >> Resultado:"+JSON.stringify (masumo2x2.multipli2x2(u,v)) + " </p>";
}



function crearMatris2x2(){

	var message = 'Digite las componentes de la Matris 2x2 separadas\
	por comas \",\" ';

	var value = prompt(message,"0,0,0,0");

	if( value != null){
		value = value.split(",");
		for (var i = 0; i < value.length; i++) {
			value[i] = parseFloat(value[i]);
		};

		if(value.length > 1){
			return new Matrix2(value[0],value[1],value[2],value[3]);
		}else{
			return new Matrix2(value[0]);
		}
	}
}



//MATRICES 3x3
var masumo3x3 = new Matrix3(null,null,null,null,null,null,null,null,null);
//BOTONES
var btnSumaMatris3x3= document.getElementById("btnSumaMatris3x3");
btnSumaMatris3x3.addEventListener('click', sumaDeMatris3x3);

var btnRestaMatris3x3= document.getElementById("btnRestaMatris3x3");
btnRestaMatris3x3.addEventListener('click', restaDeMatrices3x3);

var btnMultipliConstanteMatris3x3= document.getElementById("btnMultipliConstanteMatris3x3");
btnMultipliConstanteMatris3x3.addEventListener('click', multiplicacionConstanteDeMatrices3x3);

var btnMultiplicacionMatris3x3= document.getElementById("btnMultiplicacionMatris3x3");
btnMultiplicacionMatris3x3.addEventListener('click', multiplicacionDeMatrices3x3);



//FUNCIONES

function sumaDeMatris3x3(){
	
	var u = crearMatris3x3();
	var v = crearMatris3x3();
	masumo3x3.sumaMatrices3x3(u,v);
	consola.innerHTML += "<p> >> SUMA DE MATRICES:"+ " </p>";
	consola.innerHTML += "<p> >> Matris 1 v:"+JSON.stringify (u) + " </p>";
	consola.innerHTML += "<p> >> Matris 2 v:"+JSON.stringify (v) + " </p>";
	
	consola.innerHTML += "<p> >> Resultado:"+JSON.stringify (masumo3x3.sumaMatrices3x3(u,v)) + " </p>";
}

function restaDeMatrices3x3(){
	
	var u = crearMatris3x3();
	var v = crearMatris3x3();
	masumo3x3.restaMatrices3x3(u,v);
	consola.innerHTML += "<p> >> Matris 1 v:"+JSON.stringify (u) + " </p>";
	consola.innerHTML += "<p> >> Matris 2 v:"+JSON.stringify (v) + " </p>";
	
	consola.innerHTML += "<p> >> Resultado:"+JSON.stringify (masumo3x3.restaMatrices3x3(u,v)) + " </p>";
}

function multiplicacionConstanteDeMatrices3x3(){
	
	var u = crearMatris3x3();
	var v = crearConstante();
	masumo3x3.multiplicacionMatricesConstante3x3(u,v);
	consola.innerHTML += "<p> >> Matris 1 v:"+JSON.stringify (u) + " </p>";
	consola.innerHTML += "<p> >> Matris 2 v:"+JSON.stringify (v) + " </p>";
	
	consola.innerHTML += "<p> >> Resultado:"+JSON.stringify (masumo3x3.multiplicacionMatricesConstante3x3(u,v)) + " </p>";
}

function multiplicacionDeMatrices3x3(){
	
	var u = crearMatris3x3();
	var v = crearMatris3x3();
	masumo3x3.multiplicacionMatrices3x3(u,v);
	consola.innerHTML += "<p> >> Matris 1 v:"+JSON.stringify (u) + " </p>";
	consola.innerHTML += "<p> >> Matris 2 v:"+JSON.stringify (v) + " </p>";
	
	consola.innerHTML += "<p> >> Resultado:"+JSON.stringify (masumo3x3.multiplicacionMatrices3x3(u,v)) + " </p>";
}

function crearMatris3x3(){

	var message = 'Digite las componentes de la Matris 3x3 separadas\
	por comas \",\" ';

	var value = prompt(message,"0,0,0,0,0,0,0,0,0");

	if( value != null){
		value = value.split(",");
		for (var i = 0; i < value.length; i++) {
			value[i] = parseFloat(value[i]);
		};

		if(value.length > 1){
			return new Matrix3(value[0],value[1],value[2],value[3],value[4],value[5],value[6],value[7],value[8]);
		}else{
			return new Matrix3(value[0]);
		}
	}
}

//MATRICES 4X4
var masumo4x4 = new Matrix4(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
//BOTONES

var btnRestaMatris4x4= document.getElementById("btnSumaMatris4x4");
btnSumaMatris4x4.addEventListener('click', sumaDeMatris4x4);

var btnRestaMatris4x4= document.getElementById("btnRestaMatris4x4");
btnRestaMatris4x4.addEventListener('click', restaDeMatris4x4);

var btnMultipliConstanteMatris4x4= document.getElementById("btnMultipliConstanteMatris4x4");
btnMultipliConstanteMatris4x4.addEventListener('click', multiplicacionConstanteDeMatrices4x4);


//FUNCIONES


function sumaDeMatris4x4(){
	
	var u = crearMatris4x4();
	var v = crearMatris4x4();
	masumo4x4.sumaMatrices4x4(u,v);
	consola.innerHTML += "<p> >> Matris 1 v:"+JSON.stringify (u) + " </p>";
	consola.innerHTML += "<p> >> Matris 2 v:"+JSON.stringify (v) + " </p>";
	
	consola.innerHTML += "<p> >> Resultado:"+JSON.stringify (masumo4x4.sumaMatrices4x4(u,v)) + " </p>";
}
function restaDeMatris4x4(){
	
	var u = crearMatris4x4();
	var v = crearMatris4x4();
	masumo4x4.restaMatrices4x4(u,v);
	consola.innerHTML += "<p> >> Matris 1 v:"+JSON.stringify (u) + " </p>";
	consola.innerHTML += "<p> >> Matris 2 v:"+JSON.stringify (v) + " </p>";
	
	consola.innerHTML += "<p> >> Resultado:"+JSON.stringify (masumo4x4.restaMatrices4x4(u,v)) + " </p>";
}
function multiplicacionConstanteDeMatrices4x4(){
	
	var u = crearMatris4x4();
	var v = crearConstante();
	masumo4x4.multiplicacionMatricesConstante4x4(u,v);
	consola.innerHTML += "<p> >> Matris 1 v:"+JSON.stringify (u) + " </p>";
	consola.innerHTML += "<p> >> Matris 2 v:"+JSON.stringify (v) + " </p>";
	
	consola.innerHTML += "<p> >> Resultado:"+JSON.stringify (masumo4x4.multiplicacionMatricesConstante4x4(u,v)) + " </p>";
}



function crearMatris4x4(){

	var message = 'Digite las componentes de la Matris 4x4 separadas\
	por comas \",\" ';

	var value = prompt(message,"0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0");

	if( value != null){
		value = value.split(",");
		for (var i = 0; i < value.length; i++) {
			value[i] = parseFloat(value[i]);
		};

		if(value.length > 1){
			return new Matrix4(value[0],value[1],value[2],value[3],value[4],value[5],value[6],value[7],value[8],value[9],value[10],value[11],value[12],value[13],value[14],value[15]);
		}else{
			return new Matrix4(value[0]);
		}
	}
}