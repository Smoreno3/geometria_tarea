
function Matrix3(x1, y1, z1,    x2,y2,z2,      x3,y3,z3) {
  this.x1 = x1 || 0;
  this.y1 = y1 || 0;
  this.z1 = z1 || 0;
  this.x2 = x2 || 0;
  this.y2 = y2 || 0;
  this.z2 = z2 || 0;
  this.x3 = x3 || 0;
  this.y3 = y3 || 0;
  this.z3 = z3 || 0;
};




Matrix3.prototype = {


  set: function(x1, y1, z1,    x2,y2,z2,      x3,y3,z3 ) {
    this.x1 = x1 || 0;
    this.y1 = y1 || 0;
    this.z1 = z1 || 0;
    this.x2 = x2 || 0;
    this.y2 = y2 || 0;
    this.z2 = z2 || 0;
    this.x3 = x3 || 0;
    this.y3 = y3 || 0;
    this.z3 = z3 || 0;
    return this;
    //return [this.x1, this.y2, this.z3]
},

sumaMatrices3x3: function(v1,v2) {
    var x1 = v1.x1 + v2.x1;
    var y1 = v1.y1 + v2.y1;
    var z1 = v1.z1 + v2.z1;

    var x2 = v1.x2 + v2.x2;
    var y2 = v1.y2 + v2.y2;
    var z2 = v1.z2 + v2.z2;

    var x3 = v1.x3 + v2.x3;
    var y3 = v1.y3 + v2.y3;
    var z3 = v1.z3 + v2.z3;


    var v3 = new Matrix3(x1,y1,z1,x2,y2,z2,x3,y3,z3);
    return v3;
},
restaMatrices3x3: function(v1,v2) {
    var x1 = v1.x1 - v2.x1;
    var y1 = v1.y1 - v2.y1;
    var z1 = v1.z1 - v2.z1;

    var x2 = v1.x2 - v2.x2;
    var y2 = v1.y2 - v2.y2;
    var z2 = v1.z2 - v2.z2;

    var x3 = v1.x3 - v2.x3;
    var y3 = v1.y3 - v2.y3;
    var z3 = v1.z3 - v2.z3;


    var v3 = new Matrix3(x1,y1,z1,x2,y2,z2,x3,y3,z3);
    return v3;
},
multiplicacionMatricesConstante3x3: function(v1,T) {
    var x1 = v1.x1 *T;
    var y1 = v1.y1 *T;
    var z1 = v1.z1 *T;

    var x2 = v1.x2 *T;
    var y2 = v1.y2 *T;
    var z2 = v1.z2 *T;

    var x3 = v1.x3 *T;
    var y3 = v1.y3 *T;
    var z3 = v1.z3 *T;


    var v3 = new Matrix3(x1,y1,z1,x2,y2,z2,x3,y3,z3);
    return v3;
},
multiplicacionMatrices3x3: function(v1,v2){
    var x1 = v1.x1*v2.x1 + v1.y1*v2.x2 + v1.z1*v2.x3;
    var x2 = v1.x1*v2.y1 + v1.y1*v2.y2 + v1.z1*v2.y3;
    var x3 = v1.x1*v2.z1 + v1.y1*v2.z2 + v1.z1*v2.z3;

    var y1 = v1.x2*v2.x1 + v1.y2*v2.x2 + v1.z2*v2.x3;
    var y2 = v1.x2*v2.y1 + v1.y2*v2.y2 + v1.z2*v2.y3;
    var y3 = v1.x2*v2.z1 + v1.y2*v2.z2 + v1.z2*v2.z3;

    var z1 = v1.x3*v2.x1 + v1.y3*v2.x2 + v1.z3*v2.x3;
    var z2 = v1.x3*v2.y1 + v1.y3*v2.y2 + v1.z3*v2.y3;
    var z3 = v1.x3*v2.z1 + v1.y3*v2.z2 + v1.z3*v2.z3;

    var v3 = new Matrix3(x1,y1,z1,x2,y2,z2,x3,y3,z3);
    return v3;
    

}
};