function Matrix4(x1, y1, z1,p1,    x2,y2,z2,p2,      x3,y3,z3,p3,    x4,y4,z4,p4) {
  this.x1 = x1 || 0;
  this.y1 = y1 || 0;
  this.z1 = z1 || 0;
  this.p1 = p1 || 0;
  this.x2 = x2 || 0;
  this.y2 = y2 || 0;
  this.z2 = z2 || 0;
  this.p2 = p2 || 0;
  this.x3 = x3 || 0;
  this.y3 = y3 || 0;
  this.z3 = z3 || 0;
  this.p3 = p3 || 0;
  this.x4 = x4 || 0;
  this.y4 = y4 || 0;
  this.z4 = z4 || 0;
  this.p4 = p4 || 0;
};




Matrix4.prototype = {


  set: function(x1, y1, z1,p1,    x2,y2,z2,p2,      x3,y3,z3,p3,    x4,y4,z4,p4 ) {
      this.x1 = x1 || 0;
      this.y1 = y1 || 0;
      this.z1 = z1 || 0;
      this.p1 = p1 || 0;
      this.x2 = x2 || 0;
      this.y2 = y2 || 0;
      this.z2 = z2 || 0;
      this.p2 = p2 || 0;
      this.x3 = x3 || 0;
      this.y3 = y3 || 0;
      this.z3 = z3 || 0;
      this.p3 = p3 || 0;
      this.x4 = x4 || 0;
      this.y4 = y4 || 0;
      this.z4 = z4 || 0;
      this.p4 = p4 || 0;
      return this;
      return [this.x1, this.y2, this.z3, this.p4]
  },

  sumaMatrices4x4: function(v1,v2) {
    var x1 = v1.x1 + v2.x1;
    var y1 = v1.y1 + v2.y1;
    var z1 = v1.z1 + v2.z1;
    var p1 = v1.p1 + v2.p1;

    var x2 = v1.x2 + v2.x2;
    var y2 = v1.y2 + v2.y2;
    var z2 = v1.z2 + v2.z2;
    var p2 = v1.p2 + v2.p2;

    var x3 = v1.x3 + v2.x3;
    var y3 = v1.y3 + v2.y3;
    var z3 = v1.z3 + v2.z3;
    var p3 = v1.p3 + v2.p3;

    var x4 = v1.x4 + v2.x4;
    var y4 = v1.y4 + v2.y4;
    var z4 = v1.z4 + v2.z4;
    var p4 = v1.p4 + v2.p4;


    var v3 = new Matrix4(x1,y1,z1,p1,x2,y2,z2,p2,x3,y3,z3,p3,x4,y4,z4,p4);
    return v3;
},
restaMatrices4x4: function(v1,v2) {
   var x1 = v1.x1 - v2.x1;
   var y1 = v1.y1 - v2.y1;
   var z1 = v1.z1 - v2.z1;
   var p1 = v1.p1 - v2.p1;

   var x2 = v1.x2 - v2.x2;
   var y2 = v1.y2 - v2.y2;
   var z2 = v1.z2 - v2.z2;
   var p2 = v1.p2 - v2.p2;

   var x3 = v1.x3 - v2.x3;
   var y3 = v1.y3 - v2.y3;
   var z3 = v1.z3 - v2.z3;
   var p3 = v1.p3 - v2.p3;

   var x4 = v1.x4 - v2.x4;
   var y4 = v1.y4 - v2.y4;
   var z4 = v1.z4 - v2.z4;
   var p4 = v1.p4 - v2.p4;

   var v3 = new Matrix4(x1,y1,z1,p1,x2,y2,z2,p2,x3,y3,z3,p3,x4,y4,z4,p4);
   return v3;
},
multiplicacionMatricesConstante4x4: function(v1,T) {
    var x1 = v1.x1 *T;
    var y1 = v1.y1 *T;
    var z1 = v1.z1 *T;
    var p1 = v1.p1 *T;

    var x2 = v1.x2 *T;
    var y2 = v1.y2 *T;
    var z2 = v1.z2 *T;
    var p2 = v1.p2 *T;

    var x3 = v1.x3 *T;
    var y3 = v1.y3 *T;
    var z3 = v1.z3 *T;
    var p3 = v1.p3 *T;

    var x4 = v1.x4 *T;
    var y4 = v1.y4 *T;
    var z4 = v1.z4 *T;
    var p4 = v1.p4 *T;


    var v3 = new Matrix4(x1,y1,z1,p1,x2,y2,z2,p2,x3,y3,z3,p3,x4,y4,z4,p4);
    return v3;
}
};