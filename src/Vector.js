/**
    Vector3 Class
    **/

    var Vector3 = function( x, y, z ){

        var x = x || 0;
        this.init(x, y || x, z || x);

    };

    Vector3.prototype = {
        init: function( x, y, z ){
            this.x = x;
            this.y = y;
            this.z = z;

            return this;
        },
        constante: function(v){
            c = cF;
            return c;
        },
        multiplicacionPorConstante: function(v){
            if (v instanceof Vector3)
                return new Vector3 (this.x * v.c, this.y * v.c , this.z * v.c );
        },
        add: function(v) {
            if (v instanceof Vector3)
                return new Vector3(this.x + v.x, this.y + v.y, this.z + v.z);
            else
                return new Vector3(this.x + v, this.y + v, this.z + v);
        },
        substract: function(v){
         if (v instanceof Vector3)
            return new Vector3(this.x - v.x, this.y - v.y, this.z - v.z);
        else
            return new Vector3(this.x - v, this.y - v, this.z - v);
    },
    
    cuadrado: function(v) {
        return this.x * v.x + this.y * v.y + this.z * v.z;
    },

    moduloF: function() {
        return Math.sqrt(this.cuadrado(this))   ;
    },
    angulos: function() {
        return {
         x : Math.acos(this.x / this.moduloF())*180/Math.PI + "  grados",
         y : Math.acos(this.y / this.moduloF())*180/Math.PI + "  grados",
         z : Math.acos(this.z / this.moduloF())*180/Math.PI + "  grados"
     }
 },
 productoPunto: function(v){
    if (v instanceof Vector3)
        return (this.x * v.x + this.y * v.y + this.z * v.z);
    else
        return new Vector3(this.x * v, this.y * v, this.z * v);
},
productoCruz: function(v){
    if (v instanceof Vector3)
        return new Vector3(this.y*v.z - this.z*v.y,this.z*v.x - this.x*v.z,this.x*v.y - this.y*v.x);
    else
        return new Vector3(this.x * v, this.y * v, this.z * v);
}


};

Vector3.prototype.substract = function(){

};