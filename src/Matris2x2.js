Matrix2 = function ( n11, n12, n13, n21 ) {
  this.elements = new Float32Array(4);
  this.set(

   ( n11 !== undefined ) ? n11 : 1, n12 || 0,
   n13 || 0,( n21 !== undefined ) ? n21 : 1



   );

  
};

Matrix2.prototype = {

    //constructor: Matrix2,

    set: function ( n11, n12, n13, n21) {
      var ma = this.elements;
      ma[0] = n11; ma[3] = n21;
      ma[1] = n12; 
      ma[2] = n13; 
      return this;
    },
    sumaMatrices2x2: function(v1,v2) {
     var m = v1.elements;
     var t = v2.elements;
     var r = new Matrix2;
     return new Matrix2(
       r[0] = m[0]+t[0],
       r[1] = m[1]+t[1],
       r[2] = m[2]+t[2],
       r[3] = m[3]+t[3]


       );




   },

   restaMatrices2x2: function(v1,v2) {
     var m = v1.elements;
     var t=v2.elements;
     var r=new Matrix2;
     return new Matrix2(
       r[0] = m[0]-t[0],
       r[1] = m[1]-t[1],
       r[2] = m[2]-t[2],
       r[3] = m[3]-t[3]


       )
   },
   multipli_constante2x2: function(v1,entero) {
     var m = v1.elements;

     var r=new Matrix2;
     return new Matrix2(
       r[0] = m[0]*entero,
       r[1] = m[1]*entero,
       r[2] = m[2]*entero,
       r[3] = m[3]*entero

       )
   },


   multipli2x2: function(v1,v2) {
     var m = v1.elements;
     var t = v2.elements;
     var r = new Matrix2;
     return new Matrix2(
       r[0] = m[0]*t[0]+m[1]*t[2],
       r[1] = m[0]*t[1]+m[1]*t[3],
       r[2] = m[2]*t[0]+m[3]*t[2],
       r[3] = m[2]*t[1]+m[3]*t[3]


       )}
   };